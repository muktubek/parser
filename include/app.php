<?php
/**
 * Mini Api for manage with database
 * @author: Absatar uulu Muktubek
 * @date: 2021.01.23
 */

//error_reporting(E_ALL);
//ini_set('display_errors', 1);
ini_set('max_execution_time', 1800);
date_default_timezone_set('Asia/Bishkek');
$path = $_SERVER['DOCUMENT_ROOT'];
$domain = (isset($_SERVER['HTTPS']) ? 'https' : 'http' ). "://" . $_SERVER['SERVER_NAME'];
include 'lib/phpQuery.php';
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';
global $wpdb;

class Sites
{
	public function GetSites()
	{
		$sites = 
		[
			1 => "Lalafo",
			2 => "VB",
			3 => "Stroka",
			4 => "Doska",
			5 => "House",
			6 => "Bazar",
			7 => "Krysha",
			8 => "Domik",
			9 => "Diesel"
		];
		return $sites;
	}

	public function SignAgency($Id)
	{
		global $wpdb;
		$data = $wpdb->get_results
		(
			"SELECT * FROM wp_parser_data
		 	 WHERE (
						Title LIKE 'АН %' OR
						Description LIKE 'АН %' OR
						Title LIKE '% АН' OR
						Description LIKE '% АН' OR			
						Title LIKE 'АН. %' OR
						Description LIKE 'АН. %' OR
						Title LIKE '% АН.' OR
						Description LIKE '% АН.' OR						
						Title LIKE 'АН-%' OR
						Description LIKE 'АН-%' OR			
						Title LIKE 'АН, %' OR
						Description LIKE 'АН, %' OR			
						Title LIKE '% АН %' OR
						Description LIKE '% АН %'
					) AND Id = ".$Id
		);
		if(!empty($data))
		{
			$wpdb->update("wp_parser_data", array("IsAgency" => true), array("Id" => $Id));
		}
	}
}

class Lalafo
{
	private $url = '';

	private $type = 1;

	private $lalafoUrl = 'https://lalafo.kg';

	public function __construct($url, $type)
	{
		$this->url = $url;
		$this->type = $type;
	}

	private function GetJsonData($url)
	{
		$doc = phpQuery::newDocument(@file_get_contents($url));
		foreach($doc->find('script') as $script){
			if(pq($script)->attr('id') == '__NEXT_DATA__'){
			    $json = pq($script)->html();
			    break;
			}
		}
		return json_decode($json);
	}

	private function GetDateils($url)
	{
		$results = [];
		$jsonDataDescription = $this->GetJsonData($url);
		$details = reset($jsonDataDescription->props->initialState->feed->adDetails);
		foreach($details->item->params as $row){
			if($row->id == 69)
				$results["Rooms"] = $row->value;
			if($row->id == 70)
				$results["Area"] = $row->value;
			if($row->id == 357)
				$results["District"] = $row->value;
		}
		return $results;
	}

	public function GetData()
	{
		try
		{
			global $wpdb;
			$site = new Sites();
			$IsRunning = 0;
			$sites = $wpdb->get_results('SELECT * FROM wp_parser_sites WHERE Id = 1');
			if(!$sites[0]->IsRunning || $IsRunning == 0)
			{
				$wpdb->update('wp_parser_sites', array('IsRunning' => true), array('Id' => 1));
				for($i = 1; $i <= 1; $i++)
				{
					$url = $this->url.'&page='.$i;
					$json = '';
					$jsonData = $this->GetJsonData($url);
					$list = $jsonData->props->initialState->listing->listingFeed->items;
				    foreach($list as $item)
				    {
				    	$data = $wpdb->get_results('SELECT Id FROM wp_parser_data WHERE SiteId = 1 AND DataId = '.$item->id);
				    	if(empty($data))
				    	{
				    		$flatDateils = $this->GetDateils($this->lalafoUrl.$item->url);
				    		$IsAgency = empty($wpdb->get_results('SELECT * FROM wp_parser_agency WHERE Phone = '.trim(str_replace("+996", "", $item->mobile)))) ? false : true;
					    	$DataId = $item->id;
					    	$CategoryId = $item->category_id;
					    	$Rooms = $flatDateils["Rooms"];
					    	$Area = $flatDateils["Area"];
					    	$Series = '';
					    	$District = $flatDateils["District"];
					    	$Title = $item->title;
					    	$Description = $item->description;
					    	$Price = $item->price.''.$item->currency;
					    	$Mobile = $item->mobile;
					    	$InsertDate = date('y-m-d H:m:s');
					    	$CreateDate = date('y-m-d H:m:s', $item->created_time);
					    	$UpdateDate = date('y-m-d H:m:s', $item->updated_time);
					    	$DataUrl = $this->lalafoUrl.$item->url;
					    	$dataArr =
					    	[
					    		"SiteId" => 1,
					    		"DataId" => $DataId,
					    		"DataStrId" => '0',
					    		"CategoryId" => $CategoryId,
					    		"Rooms" => $Rooms,
					    		"Area" => $Area,
					    		"Series" => $Series,
					    		"District" => $District,
					    		"Title" => $Title,
					    		"Description" => $Description,
					    		"Price" => $Price,
					    		"Mobile" => $Mobile,
					    		"Login" => '',
					    		"InsertDate" => $InsertDate,
					    		"CreateDate" => $CreateDate,
					    		"UpdateDate" => $UpdateDate,
					    		"Url" => $DataUrl,
					    		"IsAgency" => $IsAgency,
					    		"IsExpensive" => false,
					    		"Type" => $this->type,
					    		"HttpStatus" => 200
					    	];
					    	if($wpdb->insert('wp_parser_data', $dataArr))
					    	{
					    		$site->SignAgency($wpdb->insert_id);
						    	foreach($item->images as $image)
						    	{
						    		$imageUrl = $image->original_url;
						    		$imageArr = 
						    		[
						    			"SiteId" => 1,
						    			"DataStrId" => "0",
						    		 	"DataId" => $DataId,
						    		 	"Url" => $imageUrl
						    		];
						    		$wpdb->insert('wp_parser_data_images', $imageArr);
						    	}
					    	}
				    	}
				    	else
				    	{
				    		$wpdb->update('wp_parser_data', array('UpdateDate' => date('y-m-d H:m:s', $item->updated_time)), array('DataId' => $item->id));
				    	}
				    }
				}
			    $wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 1));
			}
		}
		catch(Exception $ex)
		{
			echo $ex->getMessage();
			$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 1));
		}
	}

	public function CheckData()
	{
		try{
    		$data = $this->GetDateils($this->url);
    		if(empty($data))
    			return 404;
    		return 200;
	    }catch(Exception $ex){
	        return 404;
	    }
	}
}

class VB
{

	private $url = '';

	private $vbUrl = '';

	public function __construct($url)
	{
		$this->url = $url;
	}

	private function GetRoomsCount($url)
	{
		$rooms = [];
		for($i = 1; $i <= 4; $i++)
		{
			$path = "https://reklama.vb.kg/prodayu/".($i == 4 ? '45' : $i)."-kkvartiry/";
			$rooms[$path] = $i;
		}
		return $rooms[$url];
	}
	
	public function GetData()
	{
		try
		{
			global $wpdb;
			$site = new Sites();
			$IsRunning = 0;
			$sites = $wpdb->get_results('SELECT * FROM wp_parser_sites WHERE Id = 2');
		    if(!$sites[0]->IsRunning || $IsRunning == 0)
		    {
		    	$wpdb->update('wp_parser_sites', array('IsRunning' => true), array('Id' => 2));
				$room = $this->GetRoomsCount($this->url);
				$doc = phpQuery::newDocument(file_get_contents($this->url));
				foreach($doc->find('.ads-line') as $line)
				{
					$row = pq($line);
					$title = trim($row->text());
					$phone = trim($row->find('a')->text());
					$DataStrId = trim(preg_replace('/\s\s+/', ' ', substr($phone, -10)));
					$data = $wpdb->get_results('SELECT Id FROM wp_parser_data WHERE SiteId = 2 AND DataStrId = "'.$DataStrId.'" LIMIT 1');
					if(empty($data) && !empty($DataStrId))
					{
						$IsAgency = empty($wpdb->get_results('SELECT * FROM wp_parser_agency WHERE Phone = '.trim(substr($phone, 1)))) ? false : true;
						$DataId = 0;
						$CategoryId = 2046;
						$Rooms = $room;
						$Area = '';
						$Series = '';
						$District = '';
						$Title = $title;
						$Description = '';
						$Price = '';
						$Mobile = $phone;
						$InsertDate = date('y-m-d H:m:s');
						$CreateDate = date('y-m-d H:m:s');
						$UpdateDate = date('y-m-d H:m:s');
						$DataUrl = '#';
						$dataArr =
						[
							"SiteId" => 2,
							"DataId" => $DataId,
							"DataStrId" => $DataStrId,
							"CategoryId" => $CategoryId,
							"Rooms" => $Rooms,
							"Area" => $Area,
							"Series" => $Series,
							"District" => $District,
							"Title" => $Title,
							"Description" => $Description,
							"Price" => $Price,
							"Mobile" => $Mobile,
							"Login" => '',
							"InsertDate" => $InsertDate,
							"CreateDate" => $CreateDate,
							"UpdateDate" => $UpdateDate,
							"Url" => $DataUrl,
							"IsAgency" => $IsAgency,
							"IsExpensive" => false,
							"Type" => 1,
					    	"HttpStatus" => 200
						];
						$wpdb->insert('wp_parser_data', $dataArr);
						$site->SignAgency($wpdb->insert_id);
					}
				}
				$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 2));
			}
		}
		catch(Exception $ex)
		{
			echo $ex->getMessage();
			$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 2));
		}
	}
}

class Stroka
{
	private $url = '';

	private $strokaUrl = '';

	public function __construct($url)
	{
		$this->url = $url;
	}

	public function GetData()
	{
		try
		{
			global $wpdb;
			$site = new Sites();
			$IsRunning = 0;
			$sites = $wpdb->get_results('SELECT * FROM wp_parser_sites WHERE Id = 3');
			if(!$sites[0]->IsRunning || $IsRunning == 0)
			{
				$wpdb->update('wp_parser_sites', array('IsRunning' => true), array('Id' => 3));
				for($i = 1; $i <= 1; $i++)
				{
					$doc = phpQuery::newDocument(file_get_contents($this->url.'&p='.$i.'#paginator'));
					$results = $doc->find('.topics-item-view');
					foreach($results as $res)
					{
						$DataUrl = pq($res)->attr('href');
						$DataId = preg_replace('/[^0-9]/', '', $DataUrl);
						$data = $wpdb->get_results('SELECT Id FROM wp_parser_data WHERE SiteId = 3 AND DataId = '.$DataId);
						if(empty($data))
						{
							$pageUrl = str_replace("topic-view&topic_id", "topic-view&block&topic_id", $DataUrl);
							$pageContent = phpQuery::newDocument(file_get_contents($pageUrl));
						 	$Title = pq($pageContent->find('.topic-best-view-name'))->text();
							$Price = pq($pageContent->find('.topic-view-best-topic_cost'))->text();
							$Mobile = mb_substr(substr(preg_replace('/[^0-9]/', '', pq($pageContent->find('.topic-view-best-phone'))->text()), 1), 0, 9);
							$Rooms = pq($pageContent->find('.topic-view-best-topic_rooms'))->text();
							$Series = preg_replace('/[^0-9]/', '', pq($pageContent->find('.topic-view-best-topic_series'))->text());
							$Area = pq($pageContent->find('.topic-view-best-topic_area'))->text();
							$Description = strip_tags(pq($pageContent->find('.topic-view-body'))->html());
							$CreateDate = date('y-m-d', strtotime(preg_replace('/[^.0-9]/', '', pq($pageContent->find('.topic-view-topic_date_create_row'))->text())));
							$InsertDate = date('y-m-d H:m:s');
							$UpdateDate = date('y-m-d H:m:s');
							$CategoryId = 2046;
							$IsAgency = empty($wpdb->get_results('SELECT * FROM wp_parser_agency WHERE Phone = '.trim(str_replace("+996", "", $Mobile)))) ? false : true;
							$dataArr =
							[
								"SiteId" => 3,
								"DataId" => $DataId,
								"DataStrId" => '0',
								"CategoryId" => $CategoryId,
								"Rooms" => $Rooms,
								"Area" => $Area,
								"Series" => $Series,
								"District" => '-',
								"Title" => $Title,
								"Description" => $Description,
								"Price" => $Price,
								"Mobile" => $Mobile,
								"Login" => '',
								"InsertDate" => $InsertDate,
								"CreateDate" => $CreateDate,
								"UpdateDate" => $UpdateDate,
								"Url" => $DataUrl,
								"IsAgency" => $IsAgency,
								"IsExpensive" => false,
								"Type" => 1,
					    		"HttpStatus" => 200
							];
							if($wpdb->insert('wp_parser_data', $dataArr))
							{
								$site->SignAgency($wpdb->insert_id);
								foreach($pageContent->find('.topic-best-view-images-image-nav') as $image)
								{
									$imgText = pq($image)->attr('style');
									preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $imgText, $imagePath);
									$img = $imagePath[0][0];
									$imageArr = 
									[
										"SiteId" => 3,
						    			"DataStrId" => "0",
									   	"DataId" => $DataId,
									   	"Url" => $img
									];
									$wpdb->insert('wp_parser_data_images', $imageArr);
								}
							}
						}
					}
				}
				$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 3));
			}
		}
		catch(Exception $ex)
		{
			echo $ex->getMessage();
			$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 3));
		}
	}
}

class Doska
{
	private $url = '';

	private $doskaUrl = 'https://doska.kg';

	public function __construct($url)
	{
		$this->url = $url;
	}

	public function GetData()
	{
		try
		{
			global $wpdb;
			$site = new Sites();
			$IsRunning = 0;
			$context = stream_context_create(array('http' => array('ignore_errors' => true)));
			$sites = $wpdb->get_results('SELECT * FROM wp_parser_sites WHERE Id = 4');
			if(!$sites[0]->IsRunning || $IsRunning == 0)
			{
				$wpdb->update('wp_parser_sites', array('IsRunning' => true), array('Id' => 4));
				for($i = 1; $i <= 1; $i++)
				{
					$doc = phpQuery::newDocument(file_get_contents($this->url.'&page='.$i, false, $context));
					$results = $doc->find('.title_url');
					foreach($results as $res)
					{
						$DataUrl = $this->doskaUrl.pq($res)->attr('href');
						$DataId = preg_replace('/[^0-9]/', '', $DataUrl);
						$data = $wpdb->get_results('SELECT Id FROM wp_parser_data WHERE SiteId = 4 AND DataId = '.$DataId);
						if(empty($data))
						{
							$pageContent = phpQuery::newDocument(file_get_contents($DataUrl, false, $context));
							$Title = preg_replace('/\s+/', ' ', pq($pageContent->find('.item_title'))->text());
							$Price = pq($pageContent->find('[itemprop="price"]'))->text();					
							$Mobile = $DataId;
							$Rooms = '';
							$Series = '';
							$Area = '';
							$Description = pq($pageContent->find('[itemprop="description"]'))->text();
							$CreateDate = date('y-m-d H:m:s');
							$InsertDate = date('y-m-d H:m:s');
							$UpdateDate = date('y-m-d H:m:s');
							$CategoryId = 2046;
							$IsAgency = false;
							$dataArr =
							[
								"SiteId" => 4,
								"DataId" => $DataId,
								"DataStrId" => '0',
								"CategoryId" => $CategoryId,
								"Rooms" => $Rooms,
								"Area" => $Area,
								"Series" => $Series,
								"District" => '-',
								"Title" => $Title,
								"Description" => $Description,
								"Price" => $Price,
								"Mobile" => $Mobile,
								"Login" => '',
								"InsertDate" => $InsertDate,
								"CreateDate" => $CreateDate,
								"UpdateDate" => $UpdateDate,
								"Url" => $DataUrl,
								"IsAgency" => $IsAgency,
								"IsExpensive" => false,
								"Type" => 1,
					    		"HttpStatus" => 200
							];
							$wpdb->insert('wp_parser_data', $dataArr);
							$site->SignAgency($wpdb->insert_id);
						}
					}
				}
				$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 4));
			}
		}
		catch(Exception $ex)
		{
			echo $ex->getMessage();
			$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 4));
		}
	}

	public function CheckData()
	{
		try{
    		$context = stream_context_create(array('http' => array('ignore_errors' => true)));
    		$pageContent = phpQuery::newDocument(@file_get_contents($this->url, false, $context));
    		$Title = trim(pq($pageContent->find('.fs20'))->text());
    		if($Title == 'Объявление устарело (Возможно информация более не актуальна)')
    			return '404';
    		return '200';
	    }catch(Exception $ex){
	        return '404';
	    }
	}
}

class House
{
	private $url = '';

	private $houseUrl = 'https://www.house.kg';

	public function __construct($url)
	{
		$this->url = $url;
	}

	public function GetData()
	{
		try
		{
			global $wpdb;
			$site = new Sites();
			$IsRunning = 0;
			$sites = $wpdb->get_results('SELECT * FROM wp_parser_sites WHERE Id = 5');
			if(!$sites[0]->IsRunning || $IsRunning == 0)
			{
				$wpdb->update('wp_parser_sites', array('IsRunning' => true), array('Id' => 5));
				for($i = 1; $i <= 1; $i++)
				{
					$doc = phpQuery::newDocument(file_get_contents($this->url.'?page='.$i));
					$listing = $doc->find('.listing');
					foreach($listing as $res)
					{
						$DataUrl = $this->houseUrl.pq($res)->find('.title a')->attr('href');						
						$DataId = preg_replace('/[^0-9]/', '', explode('-',$DataUrl)[1]);
						$data = $wpdb->get_results('SELECT Id FROM wp_parser_data WHERE SiteId = 5 AND DataId = '.$DataId);
						if(empty($data))
						{
							$pageContent = phpQuery::newDocument(file_get_contents($DataUrl));
							$Title = preg_replace('/\s+/', ' ', trim(pq($res)->find('.title')->text()));
							$Price = preg_replace('/[^0-9]/', '', explode('$', pq($res)->find('.price')->text())[1]);
							$Mobile = mb_substr(preg_replace('/\s+/', '', str_replace('-', '', str_replace('+(996)','', $pageContent->find('.number')->text()))), 0, 9);
							$Rooms = '';
							$Series = '';
							$Area = '';
							$Description = pq($pageContent->find('.description p'))->text();
							$CreateDate = date('y-m-d H:m:s');
							$InsertDate = date('y-m-d H:m:s');
							$UpdateDate = date('y-m-d H:m:s');
							$CategoryId = 2046;
							$IsAgency = empty($wpdb->get_results('SELECT * FROM wp_parser_agency WHERE Phone = '.$Mobile)) ? false : true;
							
							foreach($pageContent->find('.details-main .info-row') as $row)
							{
								$label = preg_replace('/\s+/', '', pq($row)->find('.label')->text());
								$info = preg_replace('/\s+/', '', pq($row)->find('.info')->text());
								if($label == 'Серия')
									$Series = preg_replace('/[^0-9]/', '', $info);
								if($label == 'Площадь')
									$Area = preg_replace('/[^.0-9]/', '', mb_substr(str_replace(',','',str_replace('м2', '', $info)), 0, 4));
							}

							$dataArr =
							[
								"SiteId" => 5,
								"DataId" => $DataId,
								"DataStrId" => '0',
								"CategoryId" => $CategoryId,
								"Rooms" => $Rooms,
								"Area" => $Area,
								"Series" => $Series,
								"District" => '-',
								"Title" => $Title,
								"Description" => $Description,
								"Price" => $Price,
								"Mobile" => $Mobile,
								"Login" => '',
								"InsertDate" => $InsertDate,
								"CreateDate" => $CreateDate,
								"UpdateDate" => $UpdateDate,
								"Url" => $DataUrl,
								"IsAgency" => $IsAgency,
								"IsExpensive" => false,
								"Type" => 1,
					    		"HttpStatus" => 200
							];

							if($wpdb->insert('wp_parser_data', $dataArr))
							{
								$site->SignAgency($wpdb->insert_id);
								foreach($pageContent->find('[itemprop="image"]') as $image)
								{
									$img = pq($image)->attr('href');
									$imageArr = 
									[
											"SiteId" => 5,
						    				"DataStrId" => "0",
										   	"DataId" => $DataId,
										   	"Url" => $img
									];
									$wpdb->insert('wp_parser_data_images', $imageArr);
								}
							}
						}
					}
				}
				$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 5));
			}
		}
		catch(Exception $ex)
		{
			echo $ex->getMessage();
			$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 5));
		}
	}

	public function CheckData()
	{
	    try{
    		$pageContent = phpQuery::newDocument(@file_get_contents($this->url));
    		$Title = trim(pq($pageContent->find('.head-1'))->text());
    		if($Title == 'Объявление удалено')
    			return '404';
    		return '200';
	    }catch(Exception $ex){
	        return '404';
	    }
	}
}

class Bazar
{
	private $url = '';

	private $bazarUrl = 'https://www.bazar.kg';

	public function __construct($url)
	{
		$this->url = $url;
	}

	public function GetData()
	{
		try
		{
			global $wpdb;
			$site = new Sites();
			$IsRunning = 0;
			$sites = $wpdb->get_results('SELECT * FROM wp_parser_sites WHERE Id = 6');
			if(!$sites[0]->IsRunning || $IsRunning == 0)
			{
				$wpdb->update('wp_parser_sites', array('IsRunning' => true), array('Id' => 6));
				for($i = 1; $i <= 1; $i++)
				{
					$doc = phpQuery::newDocument(file_get_contents($this->url.'?page='.$i));
					$listing = $doc->find('.listing');
					foreach($listing as $res)
					{
						$DataUrl = $this->bazarUrl.pq($res)->find('.title a')->attr('href');
						$DataStrId = mb_substr(end(explode('-', $DataUrl)), 0, 10);
						$data = $wpdb->get_results('SELECT Id FROM wp_parser_data WHERE SiteId = 6 AND DataStrId = "'.$DataStrId.'" LIMIT 1');
						if(empty($data))
						{
							$pageContent = phpQuery::newDocument(file_get_contents($DataUrl));
							$Title = preg_replace('/\s+/', ' ', trim(pq($pageContent)->find('.details-header h1')->text()));
							$Price = preg_replace('/\s+/', '', str_replace('$', '', pq($pageContent)->find('.price-som')->text()));
							$Mobile = mb_substr(preg_replace('/\s+/', '', str_replace('-', '', str_replace('+(996)','', $pageContent->find('.number')->text()))), 0, 9);
							$Rooms = '';
							$Series = '';
							$Area = '';
							$Description = pq($pageContent->find('.description p'))->text();
							$CreateDate = date('y-m-d H:m:s');
							$InsertDate = date('y-m-d H:m:s');
							$UpdateDate = date('y-m-d H:m:s');
							$CategoryId = 2046;
							$IsAgency = empty($wpdb->get_results('SELECT * FROM wp_parser_agency WHERE Phone = '.$Mobile)) ? false : true;

							foreach($pageContent->find('.details-main .info-row') as $row)
							{
								$label = preg_replace('/\s+/', '', pq($row)->find('.label')->text());
								$info = preg_replace('/\s+/', '', pq($row)->find('.info')->text());
								if($label == 'Серия')
									$Series = preg_replace('/[^0-9]/', '', $info);
								if($label == 'Площадь')
									$Area = preg_replace('/[^.0-9]/', '', mb_substr(str_replace(',','',str_replace('м2', '', $info)), 0, 4));
							}

							$dataArr =
							[
								"SiteId" => 6,
								"DataId" => 0,
								"DataStrId" => $DataStrId,
								"CategoryId" => $CategoryId,
								"Rooms" => $Rooms,
								"Area" => $Area,
								"Series" => $Series,
								"District" => '-',
								"Title" => $Title,
								"Description" => $Description,
								"Price" => $Price,
								"Mobile" => $Mobile,
								"Login" => '',
								"InsertDate" => $InsertDate,
								"CreateDate" => $CreateDate,
								"UpdateDate" => $UpdateDate,
								"Url" => $DataUrl,
								"IsAgency" => $IsAgency,
								"IsExpensive" => false,
								"Type" => 1,
					    		"HttpStatus" => 200
							];
							if($wpdb->insert('wp_parser_data', $dataArr))
							{
								$site->SignAgency($wpdb->insert_id);
								foreach($pageContent->find('[itemprop="image"]') as $image)
								{
									$img = pq($image)->attr('href');
									$imageArr = 
									[
											"SiteId" => 6,
						    				"DataStrId" => $DataStrId,
										   	"DataId" => 0,
										   	"Url" => $img
									];
									$wpdb->insert('wp_parser_data_images', $imageArr);
								}
							}
						}
					}
				}
				$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 6));
			}
		}
		catch(Exception $ex)
		{
			echo $ex->getMessage();
			$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 6));
		}
	}

	public function CheckData()
	{
		try{
    		$pageContent = phpQuery::newDocument(@file_get_contents($this->url));
    		$Title = trim(pq($pageContent->find('.head-1'))->text());
    		if($Title == 'Объявление уже неактуально')
    			return '404';
    		return '200';
	    }catch(Exception $ex){
	        return '404';
	    }
	}
}

class Krysha
{
	private $url = '';

	private $kryshaUrl = 'https://krysha.kg';

	public function __construct($url)
	{
		$this->url = $url;
	}

	public function GetData()
	{
		try
		{
			global $wpdb;
			$site = new Sites();
			$IsRunning = 0;
			$sites = $wpdb->get_results('SELECT * FROM wp_parser_sites WHERE Id = 7');
			if(!$sites[0]->IsRunning || $IsRunning == 0)
			{
				$wpdb->update('wp_parser_sites', array('IsRunning' => true), array('Id' => 7));
				for($i = 1; $i <= 1; $i++)
				{
					$doc = phpQuery::newDocument(file_get_contents($this->url.'&page='.$i));
					$results = $doc->find('.c-card__link');
					foreach($results as $res)
					{
						$DataUrl = $this->kryshaUrl.pq($res)->attr('href');
						$DataId = preg_replace('/[^0-9]/','',$DataUrl);
						$data = $wpdb->get_results('SELECT Id FROM wp_parser_data WHERE SiteId = 7 AND DataId = '.$DataId);
						if(empty($data))
						{
							$pageContent = phpQuery::newDocument(file_get_contents($DataUrl));
							$Title = preg_replace('/\s+/', ' ', trim($pageContent->find('.c-details__top-heading')->text()));
							$Price = preg_replace('/[^0-9]/', '', $pageContent->find('.c-details__top-row .c-details__price--primary')->text());
							$Mobile = mb_substr(preg_replace('/[^0-9]/', '', str_replace("+996", "", $pageContent->find('.c-sticky-card__phone')->text())), 0, 9);
							$Rooms = '';
							$Series = '';
							$Area = '';
							$Description = preg_replace('/\s+/', ' ', trim(pq($pageContent->find('.c-details__desc'))->text()));
							$CreateDate = date('y-m-d H:m:s');
							$InsertDate = date('y-m-d H:m:s');
							$UpdateDate = date('y-m-d H:m:s');
							$CategoryId = 2046;
							$IsAgency = empty($wpdb->get_results('SELECT * FROM wp_parser_agency WHERE Phone = '.$Mobile)) ? false : true;

							$dataArr =
							[
								"SiteId" => 7,
								"DataId" => $DataId,
								"DataStrId" => '0',
								"CategoryId" => $CategoryId,
								"Rooms" => $Rooms,
								"Area" => $Area,
								"Series" => $Series,
								"District" => '-',
								"Title" => $Title,
								"Description" => $Description,
								"Price" => $Price,
								"Mobile" => $Mobile,
								"Login" => '',
								"InsertDate" => $InsertDate,
								"CreateDate" => $CreateDate,
								"UpdateDate" => $UpdateDate,
								"Url" => $DataUrl,
								"IsAgency" => $IsAgency,
								"IsExpensive" => false,
								"Type" => 1,
					    		"HttpStatus" => 200
							];

							if($wpdb->insert('wp_parser_data', $dataArr))
							{
								$site->SignAgency($wpdb->insert_id);
								foreach($pageContent->find('.c-gallery__img') as $image)
								{
									$img = $this->kryshaUrl.pq($image)->attr('data-flickity-lazyload');
									$imageArr = 
									[
											"SiteId" => 7,
						    				"DataStrId" => '0',
										   	"DataId" => $DataId,
										   	"Url" => $img
									];
									$wpdb->insert('wp_parser_data_images', $imageArr);
								}
							}
						}
					}
				}
				$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 7));
			}
		}
		catch(Exception $ex)
		{
			echo $ex->getMessage();
			$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 7));
		}
	}
}

class Domik
{
	private $url = '';

	private $domikUrl = 'https://krysha.kg';

	public function __construct($url)
	{
		$this->url = $url;
	}

	public function GetData()
	{
		try
		{
			global $wpdb;
			$site = new Sites();
			$IsRunning = 0;
			$arrContextOptions=array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false));
			$sites = $wpdb->get_results('SELECT * FROM wp_parser_sites WHERE Id = 8');
			if(!$sites[0]->IsRunning || $IsRunning == 0)
			{
				$wpdb->update('wp_parser_sites', array('IsRunning' => true), array('Id' => 8));
				for($i = 1; $i <= 1; $i++)
				{
					$doc = phpQuery::newDocument(file_get_contents($this->url.'/'.$i, false, stream_context_create($arrContextOptions)));
					$results = $doc->find('.title a');
					foreach($results as $res)
					{
						$DataUrl = pq($res)->attr('href');
						$DataId = preg_replace('/[^0-9]/','',$DataUrl);
						$data = $wpdb->get_results('SELECT Id FROM wp_parser_data WHERE SiteId = 8 AND DataId = '.$DataId);
						if(empty($data))
						{
							$pageContent = phpQuery::newDocument(file_get_contents($DataUrl, false, stream_context_create($arrContextOptions)));
							$Title = preg_replace('/\s+/',' ',  trim(pq($res)->text()));
							$Price = preg_replace('/[^0-9]/','', $pageContent->find('.title mark')->text());
							$Mobile = '';
							$Rooms = '';
							$Series = '';
							$Area = '';
							$Description = preg_replace('/\s+/', ' ', trim(pq($pageContent->find('.property_wrapper p'))->text()));
							$CreateDate = date('y-m-d H:m:s');
							$InsertDate = date('y-m-d H:m:s');
							$UpdateDate = date('y-m-d H:m:s');
							$CategoryId = 2046;
							$IsAgency = false;
							$dataArr =
							[
								"SiteId" => 8,
								"DataId" => $DataId,
								"DataStrId" => '0',
								"CategoryId" => $CategoryId,
								"Rooms" => $Rooms,
								"Area" => $Area,
								"Series" => $Series,
								"District" => '-',
								"Title" => $Title,
								"Description" => $Description,
								"Price" => $Price,
								"Mobile" => $Mobile,
								"Login" => '',
								"InsertDate" => $InsertDate,
								"CreateDate" => $CreateDate,
								"UpdateDate" => $UpdateDate,
								"Url" => $DataUrl,
								"IsAgency" => $IsAgency,
								"IsExpensive" => false,
								"Type" => 1,
					    		"HttpStatus" => 200
							];
							if($wpdb->insert('wp_parser_data', $dataArr))
							{
								$site->SignAgency($wpdb->insert_id);
								foreach($pageContent->find('.thumbs a') as $image)
								{
									$img = pq($image)->attr('data-preview');
									$imageArr = 
									[
											"SiteId" => 8,
						    				"DataStrId" => '0',
										   	"DataId" => $DataId,
										   	"Url" => $img
									];
									$wpdb->insert('wp_parser_data_images', $imageArr);
								}
							}
						}
					}
				}
				$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 8));
			}
		}
		catch(Exception $ex)
		{
			echo $ex->getMessage();
			$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 8));
		}
	}
}

class Diesel
{
	private $url = '';

	private $type = 1;

	private $dieselUrl = '';

	public function __construct($url, $type)
	{
		$this->url = $url;
		$this->type = $type;
	}

	public function GetRoom()
	{
		$room = '';
		$pageNumber = preg_replace('/[^0-9]/', '', $this->url);
		switch($pageNumber){
			case '459':
				$room = '1';
				break;
			case '460':
				$room = '2';
				break;
			case '461':
				$room = '3';
				break;
		}
		return $room;
	}

	public function GetData()
	{
		try
		{
			global $wpdb;
			$site = new Sites();
			$IsRunning = 0;
			$sites = $wpdb->get_results('SELECT * FROM wp_parser_sites WHERE Id = 9');
			if(!$sites[0]->IsRunning || $IsRunning == 0)
			{
				$wpdb->update('wp_parser_sites', array('IsRunning' => true), array('Id' => 9));
				for($i = 1; $i <= 1; $i++)
				{
					$doc = phpQuery::newDocument(file_get_contents($this->url.'&page='.$i));
					$results = $doc->find('[itemprop="url"]');
					foreach($results as $res)
					{
						$DataUrl = pq($res)->attr('href');
						$DataId = end(explode('=', $DataUrl));
						$data = $wpdb->get_results('SELECT Id FROM wp_parser_data WHERE SiteId = 9 AND DataId = '.$DataId);
						if(empty($data))
						{
							$pageContent = phpQuery::newDocument(file_get_contents($DataUrl));
							$Title = preg_replace('/\s+/', ' ', $pageContent->find('.ipsType_pagetitle')->text());
							$Price = preg_replace('/[^0-9]/', '', $pageContent->find('.md-price .field-value')->text());
							$Mobile = '';
							$Login = preg_split('/\s+/', pq($pageContent->find('.author_info')->find('[itemprop="name"]'))->text())[0];
							$Rooms = $this->GetRoom();
							$Series = '';
							$Area = '';
							$Description = preg_replace('/\s+/', ' ', trim(pq($pageContent->find('[itemprop="commentText"]'))->text()));
							$CreateDate = date('y-m-d H:m:s');
							$InsertDate = date('y-m-d H:m:s');
							$UpdateDate = date('y-m-d H:m:s');
							$CategoryId = 2046;
							$IsAgency = false;
							
							$dataArr =
							[
								"SiteId" => 9,
								"DataId" => $DataId,
								"DataStrId" => '0',
								"CategoryId" => $CategoryId,
								"Rooms" => $Rooms,
								"Area" => $Area,
								"Series" => $Series,
								"District" => '-',
								"Title" => $Title,
								"Description" => $Description,
								"Price" => $Price,
								"Mobile" => $Mobile,
								"Login" => $Login,
								"InsertDate" => $InsertDate,
								"CreateDate" => $CreateDate,
								"UpdateDate" => $UpdateDate,
								"Url" => $DataUrl,
								"IsAgency" => $IsAgency,
								"IsExpensive" => false,
								"Type" => $this->type,
					    		"HttpStatus" => 200
							];

							if(!empty($wpdb->get_results("SELECT * FROM wp_parser_agency_login WHERE Login = '".$Login."'")))
								continue;
							
							$wpdb->insert('wp_parser_data', $dataArr);
							$site->SignAgency($wpdb->insert_id);
						}
					}
				}
				$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 9));
			}
		}
		catch(Exception $ex)
		{
			echo $ex->getMessage();
			$wpdb->update('wp_parser_sites', array('IsRunning' => false), array('Id' => 9));
		}
	}
}

class Actualizer
{
	public static function Start()
	{
		try
		{
			global $wpdb;
			$Id = $wpdb->get_results("SELECT * FROM wp_parser_config WHERE `Key` = 'LastDataId'")[0];
			$data = $wpdb->get_results("SELECT Id, SiteId, HttpStatus, Url FROM wp_parser_data WHERE Id > ".$Id->Value." AND SiteId <> 2 AND HttpStatus <> 404 AND CategoryId <> -1 LIMIT 10");
			if(!empty($data))
			{
				foreach($data as $row)
				{
					$httpStatus = '';
					if($row->SiteId == 1)
					{
						$lalafo = new Lalafo($row->Url, 1);
						$httpStatus = $lalafo->CheckData();
					}
					if($row->SiteId == 4)
					{
						$doska = new Doska($row->Url);
						$httpStatus = $doska->CheckData();
					}
					if($row->SiteId == 5)
					{
						$house = new House($row->Url);
						$httpStatus = $house->CheckData();
					}
					if($row->SiteId == 6)
					{
						$bazar = new Bazar($row->Url);
						$httpStatus = $bazar->CheckData();
					}
					if($row->SiteId == 3 || $row->SiteId == 7 || $row->SiteId == 8 || $row->SiteId == 9)
						$httpStatus = self::CheckData($row->Url);					
					
					$wpdb->update('wp_parser_config', array('Value' => $row->Id), array('Key' => 'LastDataId'));
					if($httpStatus != '')
						$wpdb->update('wp_parser_data', array('HttpStatus' => $httpStatus), array('Id' => $row->Id));
				}
			}
			else
			{
				$wpdb->update('wp_parser_config', array('Value' => '0'), array('Key' => 'LastDataId'));
			}
		}
		catch(Exception $ex)
		{
			echo $ex->getMessage();
		}
	}

	public function CheckData($url)
	{
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $response       = curl_exec($ch);
        $errno          = curl_errno($ch);
        $error          = curl_error($ch);
        $response = $response;
        $info = curl_getinfo($ch);
		return $info['http_code'];
	}
}

if(isset($_POST['Method']))
{
	if($_POST['Method'] == 'Install')
	{
		if($wpdb->get_var("SHOW TABLES LIKE 'wp_parser_links'") != 'wp_parser_links')
		{
			$wpdb->query("DROP TABLE IF EXISTS `wp_parser_sites`");
			$wpdb->query("DROP TABLE IF EXISTS `wp_parser_links`");
			$wpdb->query("DROP TABLE IF EXISTS `wp_parser_data`");
			$wpdb->query("DROP TABLE IF EXISTS `wp_parser_data_images`");
			$wpdb->query("DROP TABLE IF EXISTS `wp_parser_agency`");
			$wpdb->query("DROP TABLE IF EXISTS `wp_parser_agency_login`");
			$wpdb->query("DROP TABLE IF EXISTS `wp_parser_config`");
			$wpdb->query("DROP TABLE IF EXISTS `wp_parser_user_report`");

			$create_table_wp_parser_sites = "
	            CREATE TABLE IF NOT EXISTS `wp_parser_sites` (
	              `Id` int NOT NULL AUTO_INCREMENT,
	              `Name` varchar(5000) NOT NULL,
	              `IsRunning` Boolean NOT NULL,
	              PRIMARY KEY (Id)
	            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	    	";
			$create_table_wp_parser_links = "
	            CREATE TABLE IF NOT EXISTS `wp_parser_links` (
	              `Id` int NOT NULL AUTO_INCREMENT,
	              `SiteId` INT NOT NULL,
	              `LinkPath` varchar(5000) NOT NULL,
	              `Type` INT NULL,	              
	              PRIMARY KEY (Id)
	            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	    	";
	    	$create_table_wp_parser_data = "
	            CREATE TABLE IF NOT EXISTS `wp_parser_data` (
	              `Id` int NOT NULL AUTO_INCREMENT,
	              `SiteId` INT NOT NULL,
	              `DataId` BIGINT NOT NULL,
	              `DataStrId` varchar(10) NOT NULL,
	              `CategoryId` INT NULL,
	              `Rooms` varchar(150) NULL,
	              `Area` varchar(15) NULL,
	              `Series` varchar(15) NULL,
	              `District` varchar(500) NULL,
	              `Title` varchar(2000) NULL,
	              `Description` TEXT NULL,
	              `Price` varchar(255) NULL,
	              `Mobile` varchar(500) NULL,
	              `Login` varchar(500) NULL,
	              `CreateDate` DATE NULL,
	              `UpdateDate` DATETIME NULL,
	              `InsertDate` DATETIME NULL,
	              `Url` varchar(5000) NOT NULL,
	              `IsAgency` Boolean NOT NULL,
	              `IsExpensive` Boolean NOT NULL,
	              `Type` INT NULL,
	              `HttpStatus` INT NULL,	              
	              PRIMARY KEY (Id)
	            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	    	";
	    	$create_table_wp_parser_data_images = "
	            CREATE TABLE IF NOT EXISTS `wp_parser_data_images` (
	              `Id` int NOT NULL AUTO_INCREMENT,
	              `SiteId` INT NULL,
	              `DataId` BIGINT NULL,
	              `DataStrId` varchar(10) NULL,
	              `Url` varchar(5000) NULL,	              
	              PRIMARY KEY (Id)
	            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	    	";
	    	$create_table_wp_parser_agency = "
	            CREATE TABLE IF NOT EXISTS `wp_parser_agency` (
	              `Id` int NOT NULL AUTO_INCREMENT,
	              `Phone` varchar(5000) NOT NULL,
				  `SiteId` INT NOT NULL,
				  `CreateDate` DATETIME NOT NULL
	              PRIMARY KEY (Id)
	            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	    	";
	    	$create_table_wp_parser_agency_login = "
	            CREATE TABLE IF NOT EXISTS `wp_parser_agency_login` (
	              `Id` int NOT NULL AUTO_INCREMENT,
	              `Login` varchar(500) NOT NULL,
				  `SiteId` INT NOT NULL,
				  `CreateDate` DATETIME NOT NULL
	              PRIMARY KEY (Id)
	            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	    	";
	    	$create_table_wp_parser_config = "
	            CREATE TABLE IF NOT EXISTS `wp_parser_config` (
	              `Id` int NOT NULL AUTO_INCREMENT,
	              `Key` varchar(500) NOT NULL,
	              `Value` varchar(500) NOT NULL,
	              `Description` varchar(500) NOT NULL,	              
	              PRIMARY KEY (Id)
	            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	    	";
	    	$create_table_wp_parser_user_report = "
	            CREATE TABLE IF NOT EXISTS `wp_parser_user_report` (
	              `Id` int NOT NULL AUTO_INCREMENT,
	              `Login` varchar(500) NOT NULL,
	              `IsAgencyCount` INT NULL,
	              `IsExpensiveCount` INT NULL,
	              `AddMyHomeCount` INT NULL,
	              `CreateDate` DATE NULL,
	              PRIMARY KEY (Id)
	            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	    	";
	    	$create_table_wp_parser_data_index = "CREATE INDEX DataIndex ON wp_parser_data (DataId, DataStrId, CreateDate)";
	    	$create_table_wp_parser_data_images_index = "CREATE INDEX DataImagesIndex ON wp_parser_data_images (DataId, DataStrId)";
	    	$create_table_wp_parser_user_report_index = "CREATE INDEX UserReport ON wp_parser_user_report (CreateDate)";
	    	
	    	$wpdb->query($create_table_wp_parser_sites);
	    	$wpdb->query($create_table_wp_parser_links);
	    	$wpdb->query($create_table_wp_parser_data);
	    	$wpdb->query($create_table_wp_parser_data_images);
	    	$wpdb->query($create_table_wp_parser_agency);
	    	$wpdb->query($create_table_wp_parser_agency_login);
	    	$wpdb->query($create_table_wp_parser_config);
	    	$wpdb->query($create_table_wp_parser_user_report);
	    	$wpdb->query($create_table_wp_parser_data_index);
	    	$wpdb->query($create_table_wp_parser_data_images_index);
	    	$wpdb->query($create_table_wp_parser_user_report_index);
	    	$agency_phones = explode(",", file_get_contents('files/agency.txt'));
	    	foreach($agency_phones as $phone)
	    	{
	    		$wpdb->insert('wp_parser_agency', array('Phone' => trim($phone)));
	    	}
	    	$sites = new Sites();
	    	foreach($sites->GetSites() as $site)
	    	{
	    		$wpdb->insert('wp_parser_sites', array("Name" => $site));
	    	}
		}
	}

	if($_POST['Method'] == 'GetPage' && empty($_SESSION['Id']))
	{
	    $page = file_get_contents('pages/login.html');
	    $data = array('Page' => $page);
	    echo Result(1, "Login", $data);
	}

	if($_POST['Method'] == 'GetPage' && !empty($_SESSION['Id']))
	{
	    $page = file_get_contents('pages/index.html');
	    $data = array("UserName" => "Вы зашли как: ".$_SESSION['Name'], "Page" => $page, "Roles" => $_SESSION['Roles']);
		echo Result(0, "OK", $data);
	}
	
	if($_POST['Method'] == 'Login')
	{
		$UserLogin = $_POST['UserLogin'];
		$UserPassword = $_POST['UserPassword'];
		$user = wp_authenticate($UserLogin, $UserPassword);
		if($user->ID > 0 && ($user->roles[0] == 'author' || $user->roles[0] == 'editor' || $user->roles[0] == 'administrator'))
		{
			$_SESSION['Id'] = $user->ID;
			$_SESSION['Name'] = $user->display_name;
			$_SESSION['Roles'] = $user->roles[0];
			$_SESSION['Login'] = $UserLogin;
		}
	}

	if($_POST['Method'] == 'LogOut')
	{
		session_destroy();
	}

	if($_POST['Method'] == 'GetData')
	{
		$siteNames = new Sites();
		$siteList = $siteNames->GetSites();
		$filterQuery = "";
		$filterTypeQuery = "";
		$filterStatusQuery = "";
		$dateBeg = $_POST['dateBeg'];
		$dateEnd = $_POST['dateEnd'];
		$filterType = $_POST['filterType'];
		$filterStatus = $_POST['filterStatus'];
		$filterLinkType = $_POST['filterLinkType'];
		if($filterType != "0")
		{
			$filterTypeQuery = "SiteId = ".$filterType;
		}
		if($filterStatus != "0")
		{
			if($filterStatus == "1")
			{
				$filterStatusQuery = "IsAgency = 1 AND IsExpensive = 0 AND HttpStatus <> 404";
			}
			if($filterStatus == "2")
			{
				$filterStatusQuery = "IsAgency = 0 AND IsExpensive = 1 AND HttpStatus <> 404";
			}
			if($filterStatus == "3")
			{
				$filterStatusQuery = "IsAgency = 0 AND IsExpensive = 0 AND HttpStatus <> 404";
			}
			if($filterStatus == "4")
			{
				$filterStatusQuery = "HttpStatus = 404";
			}
		}
		if($filterType != "0" && $filterStatus == "0")
		{
			$filterQuery = "WHERE ".$filterTypeQuery;
		}
		if($filterType == "0" && $filterStatus != "0")
		{
			$filterQuery = "WHERE ".$filterStatusQuery;
		}
		if($filterType != "0" && $filterStatus != "0")
		{
			$filterQuery = "WHERE ".$filterTypeQuery." AND ".$filterStatusQuery;
		}
		if($dateBeg != "" && $dateEnd != "")
		{
			if($filterQuery != "")
			{
				$filterQuery .= " AND CreateDate BETWEEN '".$dateBeg."' AND '".$dateEnd."'";
			}
			if($filterQuery == "")
			{
				$filterQuery = "WHERE CreateDate BETWEEN '".$dateBeg."' AND '".$dateEnd."'";
			}
		}
		if($filterQuery != "")
		{
			$filterQuery .= " AND CategoryId <> -1";
		}
		if($filterQuery == "")
		{
			$filterQuery = "WHERE CategoryId <> -1";
		}
		if($filterQuery != "")
		{
			$filterQuery .= " AND `Type` = ".$filterLinkType;
		}
		if($filterQuery == "")
		{
			$filterQuery = "WHERE `Type` = ".$filterLinkType;
		}

		echo $td = '';
		echo '<table id="Data" class="table table-striped table-bordered">
			    <thead>
			        <tr>
			            <th>К</th>
			            <th>Цена</th>
			            <th>Серия</th>
			            <th>S</th>
			            <th>Район</th>
			            <th>Описание</th>			            
			            <th>Телефон</th>
			            <th>Дата</th>
			            <th>Изменить</th>
			        </tr>
			    </thead>';
    	$data = $wpdb->get_results
		(
			'SELECT Id,
					SiteId,
					Rooms,
					Area,
					Series,
					District,
					Title,
					Price,
					Mobile,
					CreateDate,
					Url,
					IsAgency,
					IsExpensive,
					HttpStatus
			 FROM wp_parser_data '.$filterQuery
		);

		foreach($data as $row)
		{
			$style = '';
			if($row->IsAgency)
				$style = 'style="background: #f5abab;"';
			if($row->IsExpensive)
				$style = 'style="background: #e3b86f;"';
			if($row->HttpStatus == 404)
				$style = 'style="text-decoration: line-through; color: red;"';
			
			$td .= '<tr '.$style.' id='.$row->Id.'>';			
			$td .= '<td>'.(empty($row->Rooms) ? 'не указан' : preg_replace('/[^0-9]/', '', $row->Rooms)).'</td>';
			$td .= '<td>'.preg_replace('/[^0-9]/', '', $row->Price).'</td>';
			$td .= '<td>'.$row->Series.'</td>';
			$td .= '<td>'.(empty($row->Area) ? 'не указан' : $row->Area).'</td>';
			$td .= '<td>'.(empty($row->District) ? 'не указан' : mb_substr($row->District, 0, 20)).'</td>';
			$td .= '<td style="width: 280px;"><a href="#" onclick="GetDescription('.$row->Id.')" data-toggle="modal" data-target="#myModal">'.$siteList[$row->SiteId].'-'.$row->Title.'</a></td>';			
			$td .= '<td>'.$row->Mobile.'<br><a href = "'.$row->Url.'" '.($row->Url != '#' ? 'target="_blank"' : "").'>Ссылка</a></td>';
			$td .= '<td>'.date_format(date_create($row->CreateDate), 'd-m-y').'</td>';
			$td .= '<td><a style="margin-right: 15px;" id="addmyhome" onclick="AddMyHome('.$row->Id.')"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>';
			$td .= '<a style="margin-right: 15px;" id="agency" onclick="Agency('.$row->Id.')"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>';
			$td .= '<a id="expensive" onclick="Expensive('.$row->Id.')"><span class="glyphicon glyphicon-signal" aria-hidden="true"></span></a></td>';
			$td .= '</tr>';
		}
		echo $td;
		echo '<tfoot>
			        <tr>
			            <th>К</th>
			            <th>Цена</th>
			            <th>Серия</th>
			            <th>S</th>
			            <th>Район</th>
			            <th>Описание</th>			            
			            <th>Телефон</th>
			            <th>Дата</th>
			            <th>Изменить</th>
			        </tr>
			    </tfoot>';
		echo '</table>';
		?>
		<script>
			$(document).ready(function() {
				var i = 1;
			    var IdList = [];
			    var IsReuestSend = false;
			    $('#Data thead tr').clone(true).appendTo( '#Data thead' );
			    $('#Data thead tr:eq(1) th').each( function (i) {
			        var title = $(this).text();
				    $(this).html( '<input type="text" style="width: 100%;"/>' );		 
				    $( 'input', this ).on( 'keyup change', function () {
				        if ( table.column(i).search() !== this.value ) {
				            table
				                .column(i)
				                .search( this.value )
				                .draw();
				        }
				    });
			    });
			    
			    var table = $('#Data').DataTable( {
			        orderCellsTop: true,
			        fixedHeader: true
			    });
			    
			    $('#Data tr').each(function() {
			    	if($.trim(this.id) != ''){
	    				IdList[i] = this.id;
	    				i++;
    				}
  				});

  				setInterval(function(){
  					if(!IsReuestSend)
  					{
  						IsReuestSend = true;
  						$.post(url+'/app.php', {Method: 'GetActualData'}, function(data){
  							try
  							{
	  							var result = JSON.parse(data);
	  							if(result.Data.length > 0)
	  							{
			  						for(var i = 0; i <= result.Data.length-1; i++)
			  						{
			  							var row = result.Data[i];
			  							if(row.IsAgency == 1){
					                    	$('#'+row.Id).css("background-color", "");
						                    $('#'+row.Id).css("background-color", "#f5abab");
						                }
						                if(row.IsExpensive == 1){
					                    	$('#'+row.Id).css("background-color", "");
						                    $('#'+row.Id).css("background-color", "#e3b86f");
						                }
						                if(row.IsAgency == 0 && row.IsExpensive == 0)
						                	$('#'+row.Id).css("background-color", "");
			  						}
			  						for(var i = 0; i <= IdList.length-1; i++)
			  						{
			  							var IsRemove = true;
			  							for(var j = 0; j <= result.Data.length-1; j++)
			  							{
			  								if(IdList[i] == result.Data[j].Id)
			  									IsRemove = false;
			  							}
			  							if(IsRemove)
			  								$('#'+IdList[i]).remove();
			  						}
			  						IsReuestSend = false;
			  					}
		  					}
		  					catch(err)
		  					{
		  						console.log(err);
		  						IsReuestSend = false;
		  					}
  						});
  					}
  				}, 5000);
			});
		</script>
		<?php
	}

	if($_POST['Method'] == 'GetActualData')
	{
		$data = $wpdb->get_results
		(
			'SELECT Id,
					IsAgency,
					IsExpensive
			 FROM wp_parser_data'
		);
		echo Result(0, 'OK', $data);
	}

	if($_POST['Method'] == 'Description')
	{
		$img = '';
		$Id = $_POST['Id'];
		$data = $wpdb->get_results('SELECT * FROM wp_parser_data WHERE Id = '.$Id)[0];
		$images = $wpdb->get_results('SELECT * FROM wp_parser_data_images WHERE DataId > 0 AND DataId = '.$data->DataId.' AND SiteId = '.$data->SiteId);
		if(empty($images))
			$images = $wpdb->get_results('SELECT * FROM wp_parser_data_images WHERE DataStrId <> "0" AND DataStrId = "'.$data->DataStrId.'" AND SiteId = '.$data->SiteId);		
		echo '<b>Цена</b><p>'.$data->Price.'</p>';
		echo '<b>Тема</b><p>'.$data->Title.'</p>';
		echo '<b>Количество комнат</b><p>'.(empty($data->Rooms) ? 'не указан' : $data->Rooms).'</p>';
		echo '<b>Площадь</b><p>'.(empty($data->Area) ? 'не указан' : $data->Area.' кв/м').'</p>';
		echo '<b>Район</b><p>'.(empty($data->District) ? 'не указан' : $data->District).'</p>';
		echo '<b>Серия</b><p>'.(empty($data->Series) ? 'не указан' : $data->Series).'</p>';
		echo '<b>Дата публикации</b><p>'.$data->CreateDate.'</p>';
		echo '<b>Дата обновления</b><p>'.$data->UpdateDate.'</p>';
		echo '<b>Описания</b><p>'.$data->Description.'</p>';
		echo '<b>Ссылка</b><p><a href="'.$data->Url.'" target="_blank">'.$data->Url.'</a></p>';
		foreach($images as $image)
		{
			$img .= '<img src="'.$image->Url.'" width="569" height="600"/><br><hr><br>';
		}
		echo $img;
	}

	if($_POST['Method'] == 'GetUserReport')
	{
		$filter = '';
		$dateBeg = $_POST['dateBeg'];
		$dateEnd = $_POST['dateEnd'];
		$CreateDate = date('Y-m-d');
		$filter = "WHERE CreateDate = '".$CreateDate."'";
		if($dateBeg != "" && $dateEnd != "")
		{
			$filter = "WHERE CreateDate BETWEEN '".$dateBeg."' AND '".$dateEnd."'";
		}
	    $data = $wpdb->get_results
	    (
	        "SELECT x.*, u.display_name AS Name FROM 
			(
				SELECT  usr.Login,
						SUM(usr.IsAgencyCount) AS IsAgencyCount,
						SUM(usr.IsExpensiveCount) AS IsExpensiveCount,
						SUM(usr.AddMyHomeCount) AS AddMyHomeCount
				FROM wp_parser_user_report AS usr ".$filter." GROUP BY usr.Login
			) x
			JOIN mh20_users AS u ON u.user_email = x.Login ORDER BY AddMyHomeCount DESC"
	    );

		echo '<table class="table table-striped table-bordered">
			    <thead>
			        <tr>
			            <th>Имя пользователя</th>
			            <th><span class="glyphicon glyphicon-plus" aria-hidden="true" id="addmyhome"></span></th>
			            <th><span class="glyphicon glyphicon-remove" aria-hidden="true" id="agency"></span></th>
			            <th><span class="glyphicon glyphicon-signal" aria-hidden="true" id="expensive"></span></th>
			            <th>Дата</th>
			        </tr>
			    </thead>';
	    foreach($data as $row)
	    {
	    	$CreateDate = $dateBeg;
	    	if($dateBeg != $dateEnd)
	    		$CreateDate = $dateBeg.' - '.$dateEnd;
	    	echo '<tr>';
	    	echo '<td>'.$row->Name.'</td>';
	    	echo '<td>'.$row->AddMyHomeCount.'</td>';
	    	echo '<td>'.$row->IsAgencyCount.'</td>';
	    	echo '<td>'.$row->IsExpensiveCount.'</td>';
	    	echo '<td>'.$CreateDate.'</td>';
	    	echo '</tr>';
	    }
		echo '<tfoot>
			        <tr>
			            <th>Имя пользователя</th>
			            <th><span class="glyphicon glyphicon-plus" aria-hidden="true" id="addmyhome"></span></th>
			            <th><span class="glyphicon glyphicon-remove" aria-hidden="true" id="agency"></span></th>
			            <th><span class="glyphicon glyphicon-signal" aria-hidden="true" id="expensive"></span></th>
			            <th>Дата</th>
			        </tr>
			    </tfoot>';
		echo '</table>';
	}

	if($_POST['Method'] == 'AddSite')
	{
		$siteId = $_POST['siteId'];
		$siteUrl = $_POST['siteUrl'];
		$linkType = $_POST['linkType'];
		$data = 
		[
			"SiteId" => $siteId,
			"LinkPath" => $siteUrl,
			"Type" => $linkType
		];
		$wpdb->insert('wp_parser_links', $data);
	}

	if($_POST['Method'] == 'GetSites')
	{
		if($_SESSION['Roles'] == 'administrator')
		{
			$td = '';
			$siteNames = new Sites();
			$siteList = $siteNames->GetSites();
			$sites = $wpdb->get_results('SELECT * FROM wp_parser_links');
			echo '<br><br>';
			echo '<table class="table table-bordered">
		            <tr>
		              	<th>Id</th>
		                <th>Адрес сайта</th>
		                <th>Сайт</th>
		                <th>Удалить</th>
		             </tr>';
			foreach($sites as $res)
			{
	            $td .= '<tr>';
	            $td .= '<td>'.$res->Id.'</td>';
	            $td .= '<td><a href = "'.$res->LinkPath.'">'.$res->LinkPath.'</a></td>';
	            $td .= '<td>'.$siteList[$res->SiteId].'</td>';
	            $td .= '<td scope="row" style="width: 10px;"><button type="button" class="btn btn-danger" onclick="RemoveSite('.$res->Id.')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>';
	            $td .= '</tr>';
			}
			echo $td;
			echo '</table>';
		}
	}

	if($_POST['Method'] == 'DeleteSite')
	{
		$Id = $_POST['Id'];
		$wpdb->delete('wp_parser_links', array('Id' => $Id));
	}

	if($_POST['Method'] == 'AddMyHome')
	{
		$Id = $_POST['Id'];
		$siteNames = new Sites();
		$siteList = $siteNames->GetSites();
		$res = $wpdb->get_results('SELECT * FROM wp_parser_data WHERE Id = '.$Id)[0];
		if(!empty($res))
		{
			$dataPostArr = 
			[
				"post_author" => $_SESSION["Id"],
				"post_content" => $res->Description,
				"post_title" => $res->Title,
				"post_excerpt" => "",
				"post_status" => "publish",
				"comment_status" => "open",
				"ping_status" => "closed",
				"post_type" => "ad",
				"meta_input" =>
				[
					"owner_name" => "Robot",
					"owner_phone" => $res->Mobile,
					"flat_rooms" => preg_replace('/\D/', '', $res->Rooms),
					"flat_area" => $res->Area,
					"price" => preg_replace('/\D/', '', $res->Price),
					"type" => "flats",
					"_wpuf_form_id" => "8285",
					"utm_source" => $siteList[$res->SiteId]
				]
			];
			$post_id = wp_insert_post($dataPostArr);
			if($post_id > 0)
			{
				if($res->DataId != 0)
				{
					$wpdb->query('DELETE FROM wp_parser_data_images WHERE DataId = '.$res->DataId.' AND SiteId = '.$res->SiteId);
				}
				if($res->DataStrId != "0")
				{
					$wpdb->query('DELETE FROM wp_parser_data_images WHERE DataStrId = "'.$res->DataStrId.'" AND SiteId = '.$res->SiteId);
				}
				$wpdb->update("wp_parser_data", array("Rooms" => "", "Area" => "", "District" => "", "Title" => "", "Description" => "", "Url" => "", "Price" => "0", "CategoryId" => -1), array("Id" => $res->Id));
				AddUserReport('AddMyHome');
				echo Result(0, 'OK', array("post_id" => $post_id));
			}
		}
	}

	if($_POST['Method'] == 'Agency')
	{
		$IsAgency = true;
		$Id = $_POST['Id'];
		$res = $wpdb->get_results('SELECT * FROM wp_parser_data WHERE Id = '.$Id)[0];		
		if($res->IsAgency)
			$IsAgency = false;
		$agency = $wpdb->get_results("SELECT * FROM wp_parser_agency WHERE Phone = '".$res->Mobile."'");
		$agencyLogin = $wpdb->get_results("SELECT * FROM wp_parser_agency_login WHERE Login = '".$res->Login."'");
		if(!empty($res->Mobile))
		{
			if($IsAgency == false)
			{
				$wpdb->delete('wp_parser_agency', array("Phone" => $res->Mobile));
			}
			if(empty($agency) && $IsAgency == true)
			{
				$wpdb->insert('wp_parser_agency', array("Phone" => $res->Mobile));
			}
			$wpdb->update('wp_parser_data', array('IsAgency' => $IsAgency), array('Mobile' => $res->Mobile));
		}
		else
		{
			if($IsAgency == false)
			{
				$wpdb->delete('wp_parser_agency_login', array("Login" => $res->Login));
			}
			if(empty($agencyLogin) && $IsAgency == true)
			{
				$wpdb->insert('wp_parser_agency_login', array("Login" => $res->Login));
			}
			$wpdb->update('wp_parser_data', array('IsAgency' => $IsAgency), array('Id' => $res->Id));
		}
		AddUserReport('IsAgency', $IsAgency);
		echo Result(0, 'OK', array("IsAgency" => $IsAgency));
	}

	if($_POST['Method'] == 'Expensive')
	{
		$IsExpensive = true;
		$Id = $_POST['Id'];
		$res = $wpdb->get_results('SELECT * FROM wp_parser_data WHERE Id = '.$Id)[0];
		if($res->IsExpensive)
			$IsExpensive = false;
		$wpdb->update('wp_parser_data', array('IsExpensive' => $IsExpensive), array('Id' => $Id));
		AddUserReport('IsExpensive', $IsExpensive);
		echo Result(0, 'OK', array("IsExpensive" => $IsExpensive));
	}
}

if(isset($_GET['Type']) && isset($_GET['Id']))
{
	if($_GET['Type'] >= 0 && $_GET['Id'] > 0)
	{
		$Id = $_GET['Id'];
		$Type = $_GET['Type'];
		$sites = $wpdb->get_results('SELECT * FROM wp_parser_links WHERE `Type` = '.$Type.' AND `SiteId` = '.$Id);
		foreach($sites as $site)
		{
			if($site->SiteId == 1)
			{
				$lalafo = new lalafo($site->LinkPath, $site->Type);
				$lalafo->GetData();
			}
			if($site->SiteId == 2)
			{
				$vb = new VB($site->LinkPath);
				$vb->GetData();	
			}
			if($site->SiteId == 3)
			{
				$stroka = new Stroka($site->LinkPath);
				$stroka->GetData();
			}
			if($site->SiteId == 4)
			{
				$doska = new Doska($site->LinkPath);
				$doska->GetData();
			}
			if($site->SiteId == 5)
			{
				$house = new House($site->LinkPath);
				$house->GetData();
			}
			if($site->SiteId == 6)
			{
				$bazar = new Bazar($site->LinkPath);
				$bazar->GetData();
			}
			if($site->SiteId == 7)
			{
				$krysha = new Krysha($site->LinkPath);
				$krysha->GetData();
			}
			if($site->SiteId == 8)
			{
				$domik = new Domik($site->LinkPath);
				$domik->GetData();
			}
			if($site->SiteId == 9)
			{
				$diesel = new Diesel($site->LinkPath, $site->Type);
				$diesel->GetData();
			}
		}
		if($Id == 10)
		{
			$actualizer = new Actualizer();
			$actualizer->Start();
		}
	}
}

if($_POST['Method'] == 'GetPostData')
{
	$Id = $_POST['Id'];
	$path = wp_upload_dir()['basedir'];
	$postParams = array();
	$flat_seria = GetPostParam($Id, 'flat_seria');
	$postParams['Area'] = GetPostParam($Id, 'flat_area');
	$postParams['Price'] = number_format(GetPostParam($Id, 'price'), 0, '.', ' ');
	$postParams['Rooms'] = GetPostParam($Id, 'flat_rooms');
	$postParams['District'] = GetPostParam($Id, 'district');
	$postParams['Phone'] = GetUserPhone($Id);
	$post_id = get_post_meta($Id, 'images', true)[0];
	$postData = $wpdb->get_results
	(
		"SELECT img.* FROM mh20_posts AS post
		 JOIN mh20_postmeta AS img ON img.post_id = post.ID
		 WHERE img.meta_key = '_wp_attached_file' AND post.post_parent > 0
		 AND post.post_parent = ".$Id." AND img.post_id = ".$post_id." ORDER BY img.post_id"
	)[0];

	$imagePath = $path.'/'.$postData->meta_value;
	$type = pathinfo($imagePath, PATHINFO_EXTENSION);
	$data = file_get_contents($imagePath);
	$postParams['image_original_path'] = $postData->meta_value;
	$postParams['image_data'] = 'data:image/' . $type . ';base64,' . base64_encode($data);
	$postParams['flat_seria'] = !empty($flat_seria) ? $GLOBALS['listing_vars']['series_short'][$flat_seria] : 'квартира';
	$postData->meta_value = $imagePath;
	if(substr(basename($postData->meta_value), 0, 5) === 'image'){
		echo Result(1, 'Error', "Картинка уже сгенерировано");
	}else{
		echo Result(0, 'OK', array('PostData' => $postData, 'PostParams' => $postParams));
	}
}

if($_POST['Method'] == 'GetLogo')
{
	$imagePath = 'https://myhome.kg/wp-content/themes/myhomekg/images/logo.png';
	$type = pathinfo($imagePath, PATHINFO_EXTENSION);
	$data = file_get_contents($imagePath);
	$image = 'data:image/' . $type . ';base64,' . base64_encode($data);
	echo Result(0, 'OK', $image);
}

if($_POST['Method'] == 'CreateImage')
{
	$postId = $_POST['post_id'];
	$metaId = $_POST['meta_id'];
	$metaValue = $_POST['meta_value'];
	$imageData = $_POST['image_data'];
	$imageOriginalPath = $_POST['image_original_path'];
	$path = dirname($metaValue);
	$file = basename($metaValue);
	$imagePath = $path.'/image-'.$file;
	list($type, $imageData) = explode(';', $imageData);
	list(, $imageData)      = explode(',', $imageData);
	$imageData = base64_decode($imageData);
	file_put_contents($imagePath, $imageData);
	$wpdb->update('mh20_postmeta', array('meta_value' => dirname($imageOriginalPath).'/image-'.$file), array('meta_id' => $metaId, 'post_id' => $postId));
	echo Result(0, 'OK', $res);
}

if($_POST['Method'] == 'GetAgency')
{
	$sites = new Sites();
	$siteList = $sites->GetSites();
	$AgencyType = $_POST['AgencyType'];
	$AgencyTypeFormat = "'".$AgencyType."'";
	$table = $AgencyType == 'AgencyPhone' ? 'wp_parser_agency' : 'wp_parser_agency_login';
	$data = $wpdb->get_results("SELECT * FROM $table ORDER BY Id DESC");
	echo '<table class="table table-striped table-bordered">
			    <thead>
			        <tr>
			            <th>'.($AgencyType == 'AgencyPhone' ? 'Номер' : 'Логин').'</th>
			            <th>Дата</th>
			            <th>Сайт</th>
						<th>Удалить</th>
			        </tr>
			    </thead>';
	    foreach($data as $row)
	    {
	    	echo '<tr>';
			if($AgencyType == 'AgencyPhone')
			{
	    		echo '<td>'.$row->Phone.'</td>';
			}
			else
			{
				echo '<td>'.$row->Login.'</td>';
			}
			echo '<td>'.$siteList[$row->SiteId].'</td>';
	    	echo '<td>'.$row->CreateDate.'</td>';
			echo '<td scope="row" style="width: 10px;"><button type="button" class="btn btn-danger" onclick="RemoveAgency('.$row->Id.', '.$AgencyTypeFormat.')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>';
	    	echo '</tr>';
	    }
		echo '<tfoot>
			        <tr>
						<th>'.($AgencyType == 'AgencyPhone' ? 'Номер' : 'Логин').'</th>
						<th>Дата</th>
						<th>Сайт</th>
						<th>Удалить</th>
			        </tr>
			    </tfoot>';
		echo '</table>';
}

if($_POST['Method'] == 'ReomveAgency')
{
	$Id = $_POST['Id'];
	$AgencyType = $_POST['AgencyType'];
	$table = $AgencyType == 'AgencyPhone' ? 'wp_parser_agency' : 'wp_parser_agency_login';
	$wpdb->delete($table, array('Id' => $Id));
}

if($_GET['Method'] == 'AgencyExportToExcel')
{
	$sites = new Sites();
	$siteList = $sites->GetSites();
	$AgencyType = $_GET['AgencyType'];
	$AgencyHeaderName = $AgencyType == 'AgencyPhone' ? 'PhoneNumber' : 'Login';
	$AgencyTableName = $AgencyType == 'AgencyPhone' ? 'wp_parser_agency' : 'wp_parser_agency_login';
	
	// Excel file name for download 
	$fileName = $AgencyType."_" . date('Y-m-d') . ".xls";
	
	// Column names 
	$fields = array('Id', $AgencyHeaderName, 'Site', 'CreateDate');
	
	// Display column names as first row 
	$excelData = implode("\t", array_values($fields)) . "\n"; 
	
	// Fetch records from database 
	$data = $wpdb->get_results("SELECT * FROM $AgencyTableName ORDER BY Id ASC");

	if(!empty($data)){ 
		foreach($data as $row)
		{
			$PhoneOrLoginData = $AgencyType == 'AgencyPhone' ? $row->Phone : $row->Login;
			$lineData = array($row->Id, mb_convert_encoding($PhoneOrLoginData,'Windows-1251','utf-8'), $siteList[$row->SiteId], $row->CreateDate);
			array_walk($lineData, 'filterData'); 
			$excelData .= implode("\t", array_values($lineData)) . "\n";
		} 
	}else{ 
		$excelData .= 'No records found...'. "\n"; 
	} 
	
	// Headers for download
	header("Content-Type: application/vnd.ms-excel");
	header('Content-type: application/vnd.ms-excel; charset=UTF-8');
	header("Content-Disposition: attachment; filename=\"$fileName\"");
	
	// Render excel data 
	echo $excelData; 
	
	exit;
}

if($_GET['Method'] == 'ExportPhoneNumbersToExcel')
{
	$dateBeg = $_GET['dateBegPhoneNumber'];
	$dateEnd = $_GET['dateEndPhoneNumber'];

	// Excel file name for download 
	$fileName = "PhoneNumbers_" . date('Y-m-d') . ".xls";
	
	// Column names 
	$fields = array('Mobile', 'Site', 'Title');
	
	// Display column names as first row 
	$excelData = implode("\t", array_values($fields)) . "\n"; 
	
	// Fetch records from database 
	$data = $wpdb->get_results
	(
		"SELECT Mobile, Title, SiteId
		 FROM wp_parser_data 
		 WHERE CreateDate BETWEEN '".$dateBeg."' AND '".$dateEnd."' AND IsAgency <> 1 AND IsExpensive <> 1 AND LENGTH(Mobile) >= 9 AND HttpStatus <> 404"
	);

	if(!empty($data)){ 
	    $sites = new Sites();
		foreach($data as $row)
		{
			$Mobile = str_replace(' ', '', mb_convert_encoding(str_replace('996', '', preg_replace('/[^a-zA-Z0-9-]/', '', $row->Mobile)), 'Windows-1251', 'utf-8'));
			if(strlen($Mobile) == 9)
			{
			    $Site = $sites->GetSites()[$row->SiteId];
			    $Title = $row->Title;
				$lineData = array($Mobile, $Site, mb_convert_encoding($Title, 'Windows-1251', 'utf-8'));
				array_walk($lineData, 'filterData'); 
				$excelData .= implode("\t", array_values($lineData)) . "\n";
			}
		} 
	}else{
		$excelData .= 'No records found...'. "\n"; 
	}
	
	// Headers for download
	header("Content-Type: application/vnd.ms-excel");
	header('Content-type: application/vnd.ms-excel; charset=UTF-8');
	header("Content-Disposition: attachment; filename=\"$fileName\"");
	
	// Render excel data 
	echo $excelData; 
	
	exit;
}

// Filter the excel data 
function filterData(&$str){ 
    $str = preg_replace("/\t/", "\\t", $str); 
    $str = preg_replace("/\r?\n/", "\\n", $str); 
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"'; 
}

function GetPostParam($post_id, $post_meta_key) {
    global $wpdb;
    $districts =
    [
		3 => '3 мкр',
		4 => '4 мкр',
		5 => '5 мкр',
		6 => '6 мкр',
		7 => '7 мкр',
		8 => '8 мкр',
		9 => '9 мкр',
		10 => '10 мкр',
		11 => '11 мкр',
		12 => '12 мкр',
		'asan' => 'Асанбай',
		'v5' => 'Восток 5',
		'yug2' => 'Юг-2',
		'a1' => 'Аламедин 1',
		'tun' => 'Тунгуч',
		'djal' => 'Джал',
		'cen' => 'Центр',
		'zol' => 'Золотой квадрат',
		'pish' => 'Пишпек',
		'ener' => 'Городок энергетиков',
		'str' => 'Городок строителей',
		'rab' => 'Рабочий городок',
		'gaz' => 'Газ Городок',
		'jil' => 'Жилмассивы',
		'dost' => 'Достук',
		'vip' => 'VIP городки',
		'kj' => 'Кок-Жар',
		'ul' => 'Улан',
		'ul2' => 'Улан-2',
		'pol' => 'Политех',
		'fil' => 'Филармония',
		'vav' => 'Восточный Автовокзал',
		'zav' => 'Западный Автовокзал',
		'goin' => 'Гоин',
		'taat' => 'Таатан',
		'gosr' => 'Госрегистр',
		'med' => 'Медакадемия',
		'bgu' => 'БГУ',
		'mos' => 'Моссовет',
		'ortr' => 'Ортосайский рынок',
		'oshr' => 'Ошский рынок',
		'tec' => 'ТЭЦ',
		'cer' => 'Церковь',
		'cum' => 'ЦУМ'
	];
    $meta_value = '';
    $results = $wpdb->get_results
    (
        "SELECT meta_value FROM mh20_postmeta 
         WHERE meta_key = '".$post_meta_key."' AND post_id = ".$post_id
    );
    foreach($results as $result)
    {
        $meta_value = $result->meta_value;
    }
    $result = empty($meta_value) ? '1' : $meta_value;
    return !empty($result) && $post_meta_key == 'district' ? $districts[$result] : $result;
}

function GetUserPhone($post_id) {
    global $wpdb;
    $meta_value = '';
    $results = $wpdb->get_results
    (
        "SELECT userm.meta_value FROM mh20_posts AS post 
         JOIN mh20_usermeta AS userm ON userm.user_id = post.post_author
         WHERE userm.meta_key = 'phone' AND post.ID = ".$post_id
    );
    foreach($results as $result)
    {
        $meta_value = $result->meta_value;
    }
    $result = wordwrap(str_replace("+996", "0 ", $meta_value), 3, ' ',true);
    return $result;
}

function Result($Code, $Message, $Data)
{
	return json_encode(array("Code" => $Code, "Message" => $Message, "Data" => $Data));
}

function AddUserReport($ActionType, $IsAdd = true)
{
    global $wpdb;
    $CreateDate = date('Y-m-d');
    $Login = $_SESSION['Login'];
    $data = $wpdb->get_results
    (
        "SELECT * FROM wp_parser_user_report WHERE Login = '".$Login."' AND CreateDate = '".$CreateDate."'"
    )[0];
    $IsAgencyCount = (empty($data) ? 0 : $data->IsAgencyCount);
    $IsExpensiveCount = (empty($data) ? 0 : $data->IsExpensiveCount);
    $AddMyHomeCount = (empty($data) ? 0 : $data->AddMyHomeCount);
    if($ActionType == 'IsAgency' && $IsAdd == true)
    	$IsAgencyCount = $IsAgencyCount + 1;
    if($ActionType == 'IsAgency' && $IsAdd == false)
    	$IsAgencyCount = $IsAgencyCount - 1;
    if($ActionType == 'IsExpensive' && $IsAdd == true)
    	$IsExpensiveCount = $IsExpensiveCount + 1;
    if($ActionType == 'IsExpensive' && $IsAdd == false)
    	$IsExpensiveCount = $IsExpensiveCount - 1;    
    if($ActionType == 'AddMyHome')
    	$AddMyHomeCount = $AddMyHomeCount + 1;
    $IsAgencyCount = $IsAgencyCount < 0 ? 0 : $IsAgencyCount;
    $IsExpensiveCount = $IsExpensiveCount < 0 ? 0 : $IsExpensiveCount;
    if(empty($data))
    	$wpdb->insert('wp_parser_user_report', array("Login"=> $Login, "IsAgencyCount" => $IsAgencyCount, "IsExpensiveCount" => $IsExpensiveCount, "AddMyHomeCount" => $AddMyHomeCount, "CreateDate" => $CreateDate));
    if(!empty($data))
    	$wpdb->update('wp_parser_user_report', array("IsAgencyCount" => $IsAgencyCount, "IsExpensiveCount" => $IsExpensiveCount, "AddMyHomeCount" => $AddMyHomeCount), array("Login" => $Login, "CreateDate" => $CreateDate));
}

?>
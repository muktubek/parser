var url = window.location.protocol + '//' + window.location.hostname + '/pr/include';
var load = "<center><div id = 'loading'><img src = '" + url + "/img/loading.gif'/></div></center>";

function show(result, div = '.res') {
    $(div).html(result);
}

function CheckInstall(){
    show(load);
    $.post(url+'/app.php', {Method: 'Install'}, function(data){
        console.log(data);
        GetPage();
    });
}

function GetPage(){
    show(load);
    $.post(url + '/app.php', { Method: 'GetPage' }, function (data) {
        var result = JSON.parse(data);
        if(result.Code == 0)
        {
            show(result.Data.Page);
            show(result.Data.UserName, "#UserName");
            GetSites();
            if(result.Data.Roles == "administrator")
            {
                $("#admin").show();
            }
        }
        if(result.Code == 1)
        {
            show(result.Data.Page);
        }
    });
}

function GetData(){
    show(load, "#Result");
    var dateBeg = $("#dateBeg").val();
    var dateEnd = $("#dateEnd").val();
    var filterType = $("#filterType").val();
    var filterStatus = $("#filterStatus").val();
    var filterLinkType = $("#filterLinkType").val();
    $.post(url+'/app.php', {Method: 'GetData', filterType: filterType, filterStatus: filterStatus, dateBeg: dateBeg, dateEnd: dateEnd, filterLinkType: filterLinkType}, function(data){
        show(data, "#Result");
    });
}

function GetDescription(Id){
    show(load, "#Description");
    $.post(url+'/app.php', {Method: 'Description', Id: Id}, function(data){
        show(data, "#Description");
    });
}

function GetSites(){
    $.post(url+'/app.php', {Method: 'GetSites'}, function(data){
        show(data, "#SiteResult");
    });
}

function GetAgency(AgencyType)
{
    show(load, "#AgencyList");
    $.post(url+'/app.php', {Method: 'GetAgency', AgencyType: AgencyType}, function(data){
        show(data, "#AgencyList");
    });
}

function RemoveSite(Id){
    $.post(url+'/app.php', {Method: 'DeleteSite', Id: Id}, function(result){
        GetSites();
    });
}

function RemoveAgency(Id, AgencyType)
{
    var ok = confirm("Вы уверены?");
    if(ok){
        $.post(url+'/app.php', {Method: 'ReomveAgency', Id: Id, AgencyType: AgencyType}, function(data){
            $.post(url+'/app.php', {Method: 'GetAgency', AgencyType: AgencyType}, function(data){
                show(data, "#AgencyList");
            });
        });
    }
}

function AddMyHome(Id){
    var ok = confirm("Вы уверены?");
    if(ok){
        $.post(url+'/app.php', {Method: 'AddMyHome', Id: Id}, function(result){
            var res = JSON.parse(result);
            console.log(res);            
            if(res.Code == 0){
                $("#"+Id).remove();
            }
        });
    }
}

function Agency(Id){
    var ok = confirm("Вы уверены?");
    if(ok){
        $.post(url+'/app.php', {Method: 'Agency', Id: Id}, function(result){
            var res = JSON.parse(result);
            if(res.Code == 0){
                if(!res.Data.IsAgency){
                    $('#'+Id).css("background-color", "");
                }else{
                    $('#'+Id).css("background-color", "#f5abab");
                }
            }
        });
    }
}

function Expensive(Id){
    var ok = confirm("вы уверны?");
    if(ok){
        $.post(url+'/app.php', {Method: 'Expensive', Id: Id}, function(result){
            var res = JSON.parse(result);
            if(res.Code == 0){
                if(!res.Data.IsExpensive){
                    $('#'+Id).css("background-color", "");
                }else{
                    $('#'+Id).css("background-color", "#e3b86f");
                }
            }
        });
    }
}

CheckInstall();
$(function () {
    $('#btnLogin').click(function () {
        var UserLogin = $("#UserLogin").val();
        var UserPassword = $("#UserPassword").val();
        if($.trim(UserLogin) != '' && $.trim(UserPassword) != '')
        {
	        show(load);
	        $.post(url + '/app.php', { Method: 'Login', UserLogin: UserLogin, UserPassword: UserPassword }, function (data) {
                GetPage();
	        });
    	}
    });

    $('#btnLogOut').click(function(){
        var IsLogOut = confirm('Вы уверены?');
        if(IsLogOut)
        {
            $.post(url+'/app.php', {Method: 'LogOut'}, function(data){
                GetPage();
            })
        }
    });
    
    $('#btnAddSite').click(function(){
        var siteId = $('#siteId').val();
        var siteUrl = $('#siteUrl').val();
        var linkType = $('#linkType').val();
        if($.trim(siteUrl) != '' && $.trim(siteId) != '')
        {
            if(siteId == '0')
                return alert('Выберите тип сайта');
            
            if(linkType == '2' && siteId != '1' && siteId != '9')
                return alert('Добавление аренды не предусмотрена для данного сайта');

            $.post(url+'/app.php', {Method: 'AddSite', siteId: siteId, siteUrl: siteUrl, linkType: linkType}, function(result){                
                GetSites();
            })
        }
    });

    $('#btnReport').click(function(){
        show(load, "#Result");
        var dateBeg = $("#dateBeg").val();
        var dateEnd = $("#dateEnd").val();
        $.post(url+'/app.php', {Method: 'GetUserReport', dateBeg: dateBeg, dateEnd: dateEnd}, function(data){
            show(data, "#Result");
        })
    });

    $('#btnAgencyPhone, #btnAgencyLogin').click(function(){
        var Id = $(this).attr('id');
        var AgencyType = Id == 'btnAgencyPhone' ? 'AgencyPhone' : 'AgencyLogin';
        GetAgency(AgencyType);
    });

    $("#btnSearch").click(function(){
        GetData();
    })

    $('#dateBeg').datepicker({
        uiLibrary: 'bootstrap',
        format: 'yyyy-mm-dd'
    });

    $('#dateEnd').datepicker({
        uiLibrary: 'bootstrap',
        format: 'yyyy-mm-dd'
    });

    $('#dateBegPhoneNumber').datepicker({
        uiLibrary: 'bootstrap',
        format: 'yyyy-mm-dd'
    });

    $('#dateEndPhoneNumber').datepicker({
        uiLibrary: 'bootstrap',
        format: 'yyyy-mm-dd'
    });

    var date = new Date();
    var prettyDate = date.getFullYear()+'-'+GetMonth((date.getMonth()+1)) + '-' + date.getDate();
    $("#dateBeg, #dateEnd, #dateBegPhoneNumber, #dateEndPhoneNumber").val(prettyDate);

    function GetMonth(month)
    {
        return month < 10 ? "0"+month : month;
    }
});
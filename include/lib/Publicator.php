<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';
global $wpdb;

class Publicator
{	
	public static function request($url, $post, $cookie_path)
	{
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__.$cookie_path);
		curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__.$cookie_path);
		curl_exec($ch);
		curl_close($ch);
	}

	public static function upload($url, $filenames, $fields, $file_type_name, $delimiterName, $cookie_path)
	{
		$files = array();
		foreach ($filenames as $f){
		    $files[$f] = file_get_contents($f);
		}

		$curl = curl_init();
		$boundary = uniqid();
		$delimiter = $delimiterName . $boundary;
		$post_data = self::build_data_files($boundary, $fields, $files, $file_type_name, $delimiterName);

		curl_setopt_array($curl, array(
		    CURLOPT_URL => $url,
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_MAXREDIRS => 10,
		    CURLOPT_TIMEOUT => 30,
		    CURLOPT_CUSTOMREQUEST => "POST",
		    CURLOPT_POST => 1,
		    CURLOPT_POSTFIELDS => $post_data,
		    CURLOPT_HTTPHEADER => array(
		        "Content-Type: multipart/form-data; boundary=" . $delimiter,
		        "Content-Length: " . strlen($post_data)
		    )
		));
		curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__.$cookie_path);
		curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__.$cookie_path);
		$response = curl_exec($curl);
		return json_decode($response);
	}

	private static function build_data_files($boundary, $fields, $files, $file_type_name, $delimiterName){
	    $data = '';
	    $eol = "\r\n";

	    $delimiter = $delimiterName . $boundary;

	    foreach ($fields as $name => $content) {
	        $data .= "--" . $delimiter . $eol
	              . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
	              . $content . $eol;
	    }

	    foreach ($files as $name => $content) {
	        $data .= "--" . $delimiter . $eol
	              . 'Content-Disposition: form-data; name="' . $file_type_name . '"; filename="' . $name . '"' . $eol
	              . 'Content-Type: image/png'.$eol;    
	        $data .= $eol;
	        $data .= $content . $eol;
	    }
	    $data .= "--" . $delimiter . "--".$eol;

	    return $data;
	}
}

class Stroka
{
	private $login = '';
	private $password = '';
	private $data = array();
	private $cookie_path = '/cookie_stroka.txt';

	public function __construct($login, $password, $data)
	{
		$this->login = $login;
		$this->password = $password;
		$this->data = $data;
	}

	public function Publish()
	{
		$postLogin = ['login' => $this->login, 'pass' => $this->password];
		Publicator::request('https://stroka.kg/?page=action-cmd', $postLogin, $this->cookie_path);
		$images = self::upload_image();
		$postData = [
			"topic_name" => $this->data['Title'],
			"topic_cost" => $this->data['Price'],
			"topic_currency" => "dollar",
			"phones" =>  $this->data['Phone'],
			"topic_rooms" => $this->data['Rooms'],
			"topic_series" => "",
			"topic_area" => $this->data['Area'],
			"topic_floor" => $this->data['Floor'],
			"topic_floor_of" => $this->data['FloorCount'],
			"topic_repair" => "",
			"topic_furniture" => "", 
			"topic_heating" => "",
			"topic_angular" => "0",
			"topic_separate_toilet" => "0",
			"topic_gas" => "0",
			"topic_hot_water" => "0",
			"topic_water_haeter" => "0",
			"topic_an" => "no",
			"topic_body" => $this->data['Description'],
			"topic_address" => "",
			"topic_address_original" => "",
			"topic_address_google_A" => "42.86870274881194",
			"topic_address_sity" => "",
			"topic_address_google_F" => "74.59392870101931",
			"topic_address_google_zoom" => "13",
			"labels" => "1,5",
			"topic_verified" => "verified"
		];
		$postData = array_merge($postData, $images);
		Publicator::request('https://stroka.kg/?page=topic-set', $postData, $this->cookie_path);
	}

	private function upload_image()
	{
		$image_data = [];
		$url = "https://stroka.kg/?page=topic-image-add_temp";
		$images = $this->data["images"];
		$fields = array("MAX_FILE_SIZE"=>"33554432", "image_type"=>"photo");
		$file_type_name = "image_load[]";
		$delimiterName = "------WebKitFormBoundary";		
		$image_data["image_temp_sort[for_temp_id]"] = "for_image_sort";
		$image_data["image_temp_type[for_temp_id]"] = "photo";
		$image_data["image_temp_info[for_temp_id]"] = "";		
		if(!empty($images))
		{
			$res = Publicator::upload($url, $images, $fields, $file_type_name, $delimiterName, $this->cookie_path);
			if($res->success == 1)
			{
				$i = 1;
				foreach($res->data as $row)
				{
					$image_data["image_temp_sort[".$row->image_temp_id."]"] = $i;
					$image_data["image_temp_type[".$row->image_temp_id."]"] = "photo";
					$image_data["image_temp_info[".$row->image_temp_id."]"] = "";
					$i++;
				}
			}
		}
		return $image_data;
	}
}

class Doska
{
	private $login = '';
	private $password = '';
	private $data = array();
	private $cookie_path = '/cookie_doska.txt';

	public function __construct($login, $password, $data)
	{
		$this->login = $login;
		$this->password = $password;
		$this->data = $data;
	}

	public function Publish()
	{
		//$postLogin = ['_am' => $this->login, '_ap' => $this->password, '_import_redirect' => '/', '_in' => 'ru'];
		//Publicator::request('https://doska.kg/yellow.aki/regauth1.php', $postLogin, $this->cookie_path);
		$images = self::upload_image();
		$postData = [
			"cat1" => "117",
			"cat2" => "118",
			"cat_type" => "2",
			"15" => "2",
			"16" => "1",
			"param_rooms" => $this->data['Rooms'],
			"param_Smain" => $this->data['Area'],
			"param_level" => "1",
			"param_levels" => "1",
			"region" => "1",
			"param_raion_txt" => "Бишкек",
			"caption" => $this->data['Title'],
			"phone" => $this->data['Phone'],
			"owner" => "1",
			"owner_name" => "myhome.kg",
			"text" => $this->data['Description'],
			"price" => $this->data['Price'],
			"service" => "0"
		];
		foreach($images as $key => $value)
		{
			$postData[$key] = $value;
		}
		Publicator::request('https://doska.kg/add/?ajax', $postData, $this->cookie_path);
	}

	private function upload_image()
	{
		$imageId = [];
		$image_data = [];
		$url = "https://doska.kg/add/?image=upload&ajax";
		$images = $this->data["images"];
		$fields = array();
		$file_type_name = "img[]";
		$delimiterName = "------WebKitFormBoundary";		
		if(!empty($images))
		{
			$res = Publicator::upload($url, $images, $fields, $file_type_name, $delimiterName, $this->cookie_path);
			if($res->success == true)
			{
				foreach($res->data as $row)
				{
					$imageId[] = $row->thumb;
					$image_data["rotate[".$row->thumb."]"] = "";
				}
				$image_data["image"] = $imageId;
			}
		}
		return $image_data;
	}
}

class Posts
{
	private function get_posts ($limit = 10, $site_id) {
	    $args = array(
	        "post_type" => "listing",
	        "posts_per_page" => $limit,
	        "post_status" => "publish",
	        'meta_query' => 
	        [
	            [
	                'key' => $site_id.'_posts_published',
	                'compare' => 'NOT EXISTS'
	            ]
	        ],
	        'orderby' => 'date',
	        'order' => 'DECS'
	    );
	    $query = new WP_Query;
	    return $query->query($args);
	}

	private function get_post_images ($post_id) {
	    global $wpdb;
	    $urls = [];
	    $domain = (isset($_SERVER['HTTPS']) ? 'https' : 'http' ). "://" . $_SERVER['SERVER_NAME'];
	    $images = $wpdb->get_results
	    (
	        "SELECT img.meta_value FROM mh20_posts AS post
	         JOIN mh20_postmeta AS img ON img.post_id = post.ID 
	         WHERE img.meta_key = '_wp_attached_file' AND post.post_parent = ".$post_id
	    );
	    foreach($images as $image)
	    {
	        array_push($urls, $domain.'/wp-content/uploads/'.$image->meta_value);
	    }
	    return $urls;
	}

	private function get_post_param($post_id, $post_meta_key) {
	    global $wpdb;
	    $meta_value = '';
	    $results = $wpdb->get_results
	    (
	        "SELECT meta_value FROM mh20_postmeta 
	         WHERE meta_key = '".$post_meta_key."' AND post_id = ".$post_id
	    );
	    foreach($results as $result)
	    {
	        $meta_value = $result->meta_value;
	    }
	    $meta_value = !empty($meta_value) ? $meta_value : '0';
	    return $meta_value;
	}

	private function set_published ($post_id, $site_id) {
	    return add_post_meta($post_id, $site_id.'_posts_published', true, true);
	}

	private function normalize_price ($str) {
	    preg_match('/([0-9]+)\.?/', $str, $matches);
	    if (isset($matches[1])) {
	        return $matches[1];
	    }
	    return '';
	}

	private function get_post_user_phone($post_id) {
	    global $wpdb;
	    $meta_value = '';
	    $results = $wpdb->get_results
	    (
	        "SELECT userm.meta_value FROM mh20_posts AS post 
	         JOIN mh20_usermeta AS userm ON userm.user_id = post.post_author
	         WHERE userm.meta_key = 'phone' AND post.ID = ".$post_id
	    );
	    foreach($results as $result)
	    {
	        $meta_value = $result->meta_value;
	    }
	    $result = str_replace(" ", "", str_replace("+996", "0", $meta_value));
	    return $result;
	}

	private function get_post_user_meta($post_id, $post_meta_key)
	{
		global $wpdb;
		$meta_value = '';
	    $results = $wpdb->get_results
	    (
	        "SELECT userm.meta_value FROM mh20_posts AS post 
	         JOIN mh20_usermeta AS userm ON userm.user_id = post.post_author
	         WHERE userm.meta_key = '".$post_meta_key."' AND post.ID = ".$post_id
	    );
	    foreach($results as $result)
	    {
	        $meta_value = $result->meta_value;
	    }
	    $result = str_replace(" ", "", str_replace("+996", "0", $meta_value));
	    return $result;
	}

	public function get_user_list($post_id)
	{
		$users = [];
		$users['myhome.kg'] = 'qwerty123as';
	    $login = self::get_post_user_meta($post_id, 'strokakg_login');
	    $password = self::get_post_user_meta($post_id, 'strokakg_password');
	    if(!empty($login) && !empty($password))
	    {
	    	$users[$login] = $password;
	    }
	    return $users;
	}

	public static function Publish($site_id)
	{
		$posts = self::get_posts(5, $site_id);
		foreach($posts as $post)
		{
			$title = $post->post_title;
			$description = $post->post_content;
			$images = self::get_post_images($post->ID);
			$floor = self::get_post_param($post->ID, 'flat_floor');
			$floors = self::get_post_param($post->ID, 'flat_floors');
			$rooms = self::get_post_param($post->ID, 'flat_rooms');
			$area = self::get_post_param($post->ID, 'flat_area');
			$price = self::normalize_price(self::get_post_param($post->ID, 'price'));
			$phone = self::get_post_user_phone($post->ID);
			$data = 
			[
				"Title" => $title,
				"Description" => $description,
				"Price" => $price,
				"Rooms" => $rooms,
				"Area" => $area,
				"Floor" => $floor,
				"FloorCount" => $floors,
				"Phone" => $phone,
				"images" => $images
			];
			
			if($site_id == 1)
			{
    			foreach(Posts::get_user_list($post->ID) as $key => $value)
				{
					$stroka = new Stroka($key, $value, $data);
    				$stroka->Publish();
				}
			}
			
			if($site_id == 2)
			{
    			$doska = new Doska('myhome.kg', '1127966Mh', $data);
    			$doska->Publish();
			}
			self::set_published($post->ID, $site_id);
		}	
	}
}

if(isset($_GET['Id']))
{
    $Id = $_GET['Id'];
    Posts::Publish($Id);
}

?>